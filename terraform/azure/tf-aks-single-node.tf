terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id   = ""
  tenant_id         = ""
  client_id         = ""
  client_secret     = ""
  }

resource "azurerm_resource_group" "rg" {
  name      = "${var.resource_group_name_prefix}-001"
  location  = var.resource_group_location
}

resource "azurerm_kubernetes_cluster" "azlab" {
  name                = "azlab-aks1"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "azlab1"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.azlab.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.azlab.kube_config_raw

  sensitive = true
}