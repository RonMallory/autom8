terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id   = ""
  tenant_id         = ""
  client_id         = ""
  client_secret     = ""
}

resource "azurerm_resource_group" "rg" {
  name      = "${var.resource_group_name_prefix}-001"
  location  = var.resource_group_location
}

resource "azurerm_virtual_network" "azlab" {
  name      = "azlab-network"
  address_space = ["10.0.0.0/16"]
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
}

resource "azurerm_subnet" "azlab" {
  name = "internal"
  resource_group_name = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.azlab.name
  address_prefixes = ["10.0.2.0/24"]
}

resource "azurerm_subnet" "azlab-bastion"{
  name = "AzureBastionSubnet"
  resource_group_name = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.azlab.name
  address_prefixes = ["10.0.3.0/24"]
}

resource "azurerm_public_ip" "azlab" {
  name = "azlab-public"
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  allocation_method = "Static"
  sku = "Standard"
}

resource "azurerm_bastion_host" "azlab"{
  name = "azlabBastion"
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location

  ip_configuration {
    name = "configuration"
    subnet_id = azurerm_subnet.azlab-bastion.id
    public_ip_address_id = azurerm_public_ip.azlab.id
  }
}

resource "azurerm_network_interface" "azlab"{
  count = var.vm_count
  name = "azlab-nic-${count.index}"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name = "internal"
    subnet_id = azurerm_subnet.azlab.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "azlab"{
  count = var.vm_count
  name = "azlab-vm-${count.index}"
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  size = "Standard_B1s"
  admin_username = "azlabadmin"
  network_interface_ids = [
    element(azurerm_network_interface.azlab.*.id, count.index)
  ]
  admin_ssh_key {
    username = "azlabadmin"
    public_key = file("~/.ssh/id_rsa.pub")
  }
  os_disk {
    caching = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
  source_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "18.04-LTS"
    version = "latest"
  }
}
